import 'babel-polyfill'//es6转es5兼容ie
import 'url-search-params-polyfill';//让ie支持URLSearchParams
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import less from 'less'
import Runtime from '@/api/runtime'
import quillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import moment from "moment";
import * as filters from '@/utils/time'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
import VueDirectiveImagePreviewer from 'vue-directive-image-previewer'
import 'vue-directive-image-previewer/dist/assets/style.css'
Vue.use(VueDirectiveImagePreviewer) 
Vue.prototype.$moment = moment;
Vue.prototype.$runtime = Runtime

Vue.use(ElementUI);
Vue.use(less)
Vue.config.productionTip = false
Vue.use(quillEditor)

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
