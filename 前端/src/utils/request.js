//axios的封装
import axios from 'axios' //引入axios

//创建一个axios实例
var service = axios.create({
  // baseURL: '/api',
  timeout: '5000'
})
////定义一个拦截器
// 添加请求拦截器
service.interceptors.request.use(config => {
  //在发送请求之前做些什么
  // var token = "lanqiao123456789"
  // if (token) {
  //     config.headers['Authorization'] = "Bearer " + token;
  // }
  // if (config.method == 'post') {
  //     config.headers['Content-Type'] = 'application/json,charset=utf-8';
  // }
  return config;
}, error => {
  // 对请求错误做些什么
  return Promise.reject(error);
})
//// 添加响应拦截器
service.interceptors.response.use(response => {
  // 对响应数据做点什么
  const data = response.data;
  switch (data.code) {
    case 200:
    case 201:
    case 204:
      return data; //返回data
    default:
      //弹框
      // this.$message({
      //   message: data.message,
      //   type: 'warning'
      // });
      return Promise.reject(data.message);
  }
  return response;
}, error => {
  // 对响应错误做点什么
  return Promise.reject(error);
})
export default service;