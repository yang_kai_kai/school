import $moment from 'moment'

const timeFormat = (value, format) => {
  return value == null ? '' : $moment(value).format(format)
}
export {
  timeFormat
}
