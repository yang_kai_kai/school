import request from './request' // 导入封装好的axios实例
// let load = document.querySelector(".load");
const http = {

    /**
     * methods: 请求
     * @param {*} url 
     * @param {*} params 
     */
    get(url, params) {
        const config = {
            method: 'get',
            url: url
        }
        if (params) config.params = params
            // load.style.display = "block";
        return request(config)
    },
    post(url, params) {
        const config = {
            method: 'post',
            url: url
        }
        if (params) config.data = params
        return request(config)
    },
    put(url, params) {
        const config = {
            method: 'put',
            url: url
        }
        if (params) config.params = params
        return request(config)
    },
    delete(url, params) {
        const config = {
            method: 'delete',
            url: url
        }
        if (params) config.data = params
        return request(config)
    },
    patch(url, params) {
        const config = {
            method: 'patch',
            url: url
        }
        if (params) config.data = params
        return request(config)
    },
    file(url,params) {
      const config = {
        method:"post",
        url:url,
        headers: {
          'content-type': 'multipart/form-data',
        },
      }
      if (params) config.data = params
      return request(config)
    }
}
export default http;