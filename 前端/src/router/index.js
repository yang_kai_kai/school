import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}
let router = new VueRouter({
  mode: "hash", //1、hash哈希：有#号。2、history历史：没有#号
  base: process.env.BASE_URL,
  //记录滚动的位置解决白屏问题，必须配合keep-alive
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: "/login",
      name: "login",
      component: () => import('../views/login'),
      meta: { keepAlive: true, title: "首页" }
    },
    {
      path: "/register",
      name: "register",
      component: () => import('../views/register'),
      meta: { keepAlive: true, title: "注册" }
    },
    {
      path: "/loginAdmiin",
      name: "loginAdmiin",
      component: () => import('../views/loginAdmiin'),
      meta: { keepAlive: true, title: "后台登录" }
    },
    {
      path: "/admin",
      name: "admin",
      component: () => import('../views/admin/nav'),
      redirect: "/home",
      children: [
        {
          path: '/home',
          name: 'home',
          component: () => import('../views/admin/home'),
          meta: { keepAlive: true, title: "首页" }
        }, {
          path: '/user',
          name: 'user',
          component: () => import('../views/admin/user'),
          meta: { keepAlive: true, title: "首页" }
        }, {
          path: '/fengmian',
          name: 'fengmian',
          component: () => import('../views/admin/fengmian'),
          meta: { keepAlive: true, title: "轮播管理" }
        }, {
          path: '/news',
          name: 'news',
          component: () => import('../views/admin/news'),
          meta: { keepAlive: true, title: "新闻动态" }
        }, {
          path: '/teacher',
          name: 'teacher',
          component: () => import('../views/admin/teacher'),
          meta: { keepAlive: true, title: "教师风采" }
        }, {
          path: '/alumni',
          name: 'alumni',
          component: () => import('../views/admin/alumni'),
          meta: { keepAlive: true, title: "校友风采" }
        }, {
          path: '/campus',
          name: 'campus',
          component: () => import('../views/admin/campus'),
          meta: { keepAlive: true, title: "校园风光" }
        }, {
          path: '/graduationphoto',
          name: 'graduationphoto',
          component: () => import('../views/admin/graduationphoto'),
          meta: { keepAlive: true, title: "毕业季" }
        }, {
          path: '/donates',
          name: 'donates',
          component: () => import('../views/admin/donates'),
          meta: { keepAlive: true, title: "捐赠项目" }
        }, {
          path: '/yuyue',
          name: 'yuyue',
          component: () => import('../views/admin/yuyue'),
          meta: { keepAlive: true, title: "首页" }
        },]
    },
    {
      path: "/",
      name: "main",
      component: () => import('../views/publicNav/MainContainer'),
      redirect: "/index",
      children: [
        {
          path: '/index',
          name: 'index',
          component: () => import('../views/main/index'),
          meta: { keepAlive: true, title: "首页" }
        },
        //捐赠项目
        {
          path: 'main/donate',
          name: 'donate',
          component: () => import('../views/main/donate'),
          meta: { keepAlive: true, title: "首页" }
        },
        //个人中心
        {
          path: '/person',
          name: 'person',
          component: () => import('../views/main/person'),
          meta: { keepAlive: true, title: "首页" }
        },
        //新闻动态
        {
          path: 'main/newsInfo',
          name: 'newsInfo',
          component: () => import('../views/main/newsInfor'),
          meta: { keepAlive: true, title: "新闻动态" }
        },
        //新闻动态详情
        {
          path: 'main/newsInfo/newsDetails/:index',
          name: 'newsDetails',
          component: () => import('../views/main/newsInfor/newsDetails.vue'),
          meta: { keepAlive: true, title: "新闻动态详情" }
        },
        {
          path: 'main/aboutUs',
          name: 'aboutUs',
          component: () => import('../views/main/aboutUs'),
          meta: { keepAlive: true, title: "关于我们" }
        },
        {
          path: 'main/commonPro',
          name: 'commonPro',
          component: () => import('../views/main/commonPro'),
          meta: { keepAlive: true, title: "常见问题" }
        },
        //校友会概括子路由
        {
          path: 'main/synopsis',
          name: 'synopsis',
          component: () => import('../views/main/alumAssocSum/synopsis'),
          meta: { keepAlive: true, title: "校友会简介" }
        },
        {
          path: 'main/rules',
          name: 'rules',
          component: () => import('../views/main/alumAssocSum/rules'),
          meta: { keepAlive: true, title: "规章制度" }
        },
        {
          path: 'main/developHistory',
          name: 'developHistory',
          component: () => import('../views/main/alumAssocSum/developHistory'),
          meta: { keepAlive: true, title: "发展历程" }
        },
        {
          path: 'main/aluminOffice',
          name: 'aluminOffice',
          component: () => import('../views/main/alumAssocSum/aluminOffice'),
          meta: { keepAlive: true, title: "校友办公室" }
        },
        //风采展示子路由
        {
          path: 'main/teacherMien',
          name: 'teacherMien',
          component: () => import('../views/main/mienShow/teacherMien'),
          meta: { keepAlive: true, title: "教师风采" }
        },
        {
          path: 'main/alumnaMein',
          name: 'alumnaMein',
          component: () => import('../views/main/mienShow/alumnaMein'),
          meta: { keepAlive: true, title: "校友风采" }
        },
        {
          path: 'main/schoolView',
          name: 'schoolView',
          component: () => import('../views/main/mienShow/schoolView'),
          meta: { keepAlive: true, title: "校园风光" }
        },
        {
          path: 'main/schoolHistory',
          name: 'schoolHistory',
          component: () => import('../views/main/mienShow/schoolHistory'),
          meta: { keepAlive: true, title: "校园历史" }
        },
        //教师风采详情
        {
          path: 'main/teacherMien/teachmienDetail/:index',
          name: 'mienDetail',
          component: () => import('../views/main/mienShow/teacherMien/teachmienDetail.vue'),
          meta: { keepAlive: true, title: "教师风采详情" }
        },
        //校友风采详情
        {
          path: 'main/alumnaMein/alumnMeinDetail/:index',
          name: 'alumnMeinDetail',
          component: () => import('../views/main/mienShow/alumnaMein/alumnMeinDetail.vue'),
          meta: { keepAlive: true, title: "校友风采详情" }
        },
        //校园风光详情
        {
          path: 'main/schoolView/schoolViewDetail/:index',
          name: 'schoolViewDetail',
          component: () => import('../views/main/mienShow/schoolView/schoolViewDetail.vue'),
          meta: { keepAlive: true, title: "校园风光详情" }
        },
        //服务大厅子路由
        {
          path: 'main/applictSchool',
          name: 'applictSchool',
          component: () => import('../views/main/serviceHall/applictSchool'),
          meta: { keepAlive: true, title: "返校服务" }
        },
        {
          path: 'main/consultService',
          name: 'consultService',
          component: () => import('../views/main/serviceHall/consultService'),
          meta: { keepAlive: true, title: "精品生活" }
        },
        //精品生活详情
        {
          path: 'main/consultService/consultServiceDetail/:index',
          name: 'consultServiceDetail',
          component: () => import('../views/main/serviceHall/consultService/consultServiceDetail.vue'),
          meta: { keepAlive: true, title: "精品生活详情" }
        },
        {
          path: 'main/alumnCard',
          name: 'alumnCard',
          component: () => import('../views/main/serviceHall/alumnCard'),
          meta: { keepAlive: true, title: "校友卡" }
        },
        {
          path: 'main/download',
          name: 'download',
          component: () => import('../views/main/serviceHall/download'),
          meta: { keepAlive: true, title: "常用下载" }
        },
        //校友捐赠子路由
        {
          path: 'main/foundation',
          name: 'foundation',
          component: () => import('../views/main/donation/foundation'),
          meta: { keepAlive: true, title: "基金会介绍" }
        },
        {
          path: 'main/donatNews',
          name: 'donatNews',
          component: () => import('../views/main/donation/donatNews'),
          meta: { keepAlive: true, title: "捐赠动态" }
        },
        //捐赠动态详情
        {
          path: 'main/donatNews/donatNewsDetail/:index',
          name: 'donatNewsDetail',
          component: () => import('../views/main/donation/donatNews/donatNewsDetail.vue'),
          meta: { keepAlive: true, title: "捐赠动态详情" }
        },
        {
          path: 'main/donatItem',
          name: 'donatItem',
          component: () => import('../views/main/donation/donatItem'),
          meta: { keepAlive: true, title: "捐赠项目" }
        },
        //捐赠项目详情
        {
          path: 'main/donatItem/donatItemDetail',
          name: 'donatItemDetail',
          component: () => import('../views/main/donation/donatItem/donatItemDetail'),
          meta: { keepAlive: true, title: "捐赠项目详情" }
        },
        {
          path: 'main/onlineDonat',
          name: 'onlineDonat',
          component: () => import('../views/main/donation/onlineDonat'),
          meta: { keepAlive: true, title: "捐赠指南" }
        },
        {
          path: 'main/otherDonat',
          name: 'otherDonat',
          component: () => import('../views/main/donation/otherDonat'),
          meta: { keepAlive: true, title: "捐赠免税" }
        },
      ]
    },

  ]
});

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    if (Boolean(localStorage['isLogin'])) {
      next();
    } else {
      next("/");
    }
  } else {
    next();
  }
});

export default router
