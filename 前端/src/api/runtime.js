const RuntimeConfig = function (options) {
  options = options || {}
  return '智能提示：前端运行环境配置'
}

// 正式环境
// 后端接口路径
RuntimeConfig.UPLOAD = 'http://127.0.0.1:8080/schoolfellow/fileupload/upLoadLocal'
RuntimeConfig.PREVIEW = 'http://127.0.0.1:8080/'

export default RuntimeConfig
