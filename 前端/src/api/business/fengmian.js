import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/FengmianController/selectAllByLayui', data, fun, true, true)

}

export const add = (data, fun) => {
  request.post('/FengmianController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/FengmianController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/FengmianController/delete', data, fun, true, true)
}