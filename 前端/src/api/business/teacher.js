import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/TeacherController/selectAllByLayui', data, fun, true, true)
}

export const selectTeacher = (data, fun) => {
  request.get('/TeacherController/selectAllByLayui', data, fun, true, true)
}
export const selectOne = (data, fun) => {
  request.get('/TeacherController/selectOne', data, fun, true, true)
}
export const add = (data, fun) => {
  request.post('/TeacherController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/TeacherController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/TeacherController/delete', data, fun, true, true)
}