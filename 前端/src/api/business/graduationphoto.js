import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/QualityserviceController/selectAllByLayui', data, fun, true, true)

}
export const selectOne = (data, fun) => {
  request.get('/QualityserviceController/selectOne', data, fun, true, true)
}
export const selectAlumni = (data, fun) => {
  request.get('/QualityserviceController/selectAllByLayui', data, fun, true, true)
}

export const add = (data, fun) => {
  request.post('/QualityserviceController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/QualityserviceController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/QualityserviceController/delete', data, fun, true, true)
}