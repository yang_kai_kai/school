import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/SubscribeController/selectAllByLayui', data, fun, true, true)

}
export const selectOne = (data, fun) => {
  request.get('/SubscribeController/selectOne', data, fun, true, true)
}
export const selectAlumni = (data, fun) => {
  request.get('/SubscribeController/selectAllByLayui', data, fun, true, true)
}
export const selectMyself = (data, fun) => {
  request.get('/SubscribeController/selectMyself', data, fun, true, true)
}
export const add = (data, fun) => {
  request.post('/SubscribeController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/SubscribeController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/SubscribeController/delete', data, fun, true, true)
}