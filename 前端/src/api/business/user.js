import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/user/selectAllByLayui', data, fun, true, true)

}

export const add = (data, fun) => {
  request.post('/user/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/user/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/user/delete', data, fun, true, true)
}