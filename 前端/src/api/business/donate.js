import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/DonateController/selectAllByLayui', data, fun, true, true)

}
export const selectOne = (data, fun) => {
  request.get('/DonateController/selectOne', data, fun, true, true)
}
export const selectAlumni = (data, fun) => {
  request.get('/DonateController/selectAllByLayui', data, fun, true, true)
}

export const add = (data, fun) => {
  request.post('/DonateController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/DonateController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/DonateController/delete', data, fun, true, true)
}