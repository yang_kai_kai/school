import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/NewsController/selectAllByLayui', data, fun, true, true)
}

export const selectNews = (data, fun) => {
  request.get('/NewsController/selectAllByLayui', data, fun, true, true)
}


export const selectOne = (data, fun) => {
  request.get('/NewsController/selectOne', data, fun, true, true)
}

export const add = (data, fun) => {
  request.post('/NewsController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/NewsController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/NewsController/delete', data, fun, true, true)
}