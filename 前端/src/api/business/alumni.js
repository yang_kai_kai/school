import request from '../api'

export const selectAllByLayui = (data, fun) => {
  request.get('/AlumniController/selectAllByLayui', data, fun, true, true)

}
export const selectOne = (data, fun) => {
  request.get('/AlumniController/selectOne', data, fun, true, true)
}
export const selectAlumni = (data, fun) => {
  request.get('/AlumniController/selectAllByLayui', data, fun, true, true)
}

export const add = (data, fun) => {
  request.post('/AlumniController/add', data, fun, true, true)

}

export const update = (data, fun) => {
  request.post('/AlumniController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/AlumniController/delete', data, fun, true, true)
}