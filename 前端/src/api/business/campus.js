import request from '../api'

export const schoolAllByLayui = (data, fun) => {
  request.get('/CampusController/selectAllByLayui', data, fun, true, true)
}
export const selectOne = (data, fun) => { 
  request.get('/CampusController/selectOne', data, fun, true, true)

}
export const add = (data, fun) => {
  request.post('/CampusController/add', data, fun, true, true)
}

export const update = (data, fun) => {
  request.post('/CampusController/update', data, fun, true, true)
}

export const deleteById = (data, fun) => {
  request.get('/CampusController/delete', data, fun, true, true)
}