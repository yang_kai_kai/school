import config from './config'
import http from './http'
// import {getIP} from './server'//服务IP配置
/**
 * 使用接口请求
 * 日期 ：2018/9/12
 ***/
// ouyi                    http://192.168.13.17:8080/zservice/
//尤艳明服务器old      http://192.168.13.26:8888/mydemo_Web_exploded/api/

let APIIP = ""
if (process.env.NODE_ENV == "development") {//开发环境
  APIIP = "http://127.0.0.1:8080/schoolfellow"
  //APIIP="http://192.168.13.19:8080/zservice/"//尤艳明
  //APIIP="http://192.168.13.17:8080/zservice/"//ouyi
} else if (process.env.NODE_ENV == "production") {//正式环境
  //APIIP="http://api.vue.my.zmeth.com/zservice/"
  APIIP = "http://127.0.0.1:8080/schoolfellow"
}

export default {

  /*post方法
  *标准的post传值方法，参数是obj表单 对象时的处理方法 参数是赋值到 data上
  * url:API接口路径
  * data:                   json对象数据，这里只是接收，在config.js transformRequest中转换成string
  * options：           回调函数，因为是JS异步处理，所以需要将处理函数传过来
  * isCheckToken:   是否开起token验证，如登录、注册、请求新token、非登录网页展示这类API不需要验证，
  *                           其他项目内API需要打开，可缺省参数，默认打开验证
  * isloading:          是否使用全局loading，可缺省参数，默认false，如果需要触发遮罩层，传ture
  */
  post: function (url, data, options, isCheckToken, isloading) {
    config.url = APIIP + url;//this.initConfig.ExaminationPlatformUrl+url;
    config.data = JSON.stringify(data);
    config.params = ""
    isCheckToken === undefined ? config.interceptor = true : config.interceptor = isCheckToken;//默认打开验证
    http.request("POST", config, options, options, isloading); 
  },
  /*
  * postG方法
  * 用于替代之前get方法，主要使用场景为参数<4的 '非整表对象'的情况，参数是赋值到 params 以url？id=xxx形式传输的
  * 用于：登录，通过某个ID进行查询、通过ID单条删除等
  * 参数同post
  */
  postG: function (url, data, options, isCheckToken, isloading) {
    config.url = APIIP + url;
    config.data = JSON.stringify(data);
    config.params = data
    isCheckToken === undefined ? config.interceptor = true : config.interceptor = isCheckToken;//默认打开验证
    http.request("POST", config, options, options, isloading);
  },
  //get请求
  //将token值保存到localStorage中
  get: function (url, params, options, isCheckToken, isloading) {
    config.url = APIIP + url;
    config.data = "";
    config.params = params;//axios专门用于装get请求参数的
    isCheckToken === undefined ? config.interceptor = true : config.interceptor = isCheckToken;//默认打开验证
    http.request("GET", config, options, options, isloading);
  },

  postFile: function (url, data, options, isCheckToken, isloading) {
    config.url = APIIP + url;
    // 把参数处理成表达提交格式
    config.data = data;
    config.headers['Content-Type'] = "application/x-www-form-urlencoded";
    isCheckToken === undefined ? config.interceptor = true : config.interceptor = isCheckToken;//默认打开验证
    http.request("POST", config, options, options, isloading);
  },
  getIP: function (url) {
    return APIIP + url
  }
}
