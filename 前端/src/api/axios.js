import axios from 'axios'
import router from '@/router/index'
import request from './api'//服务IP配置
// import { Message } from "element-ui";
import {getUTCTime} from './timeUtil'

let isTokenRefreshing = false;// 当标志为true 代表程序正在获取新token
let subscribesArr = [];// 存储前端发送请求的数组
const tokenRequestTime=100;//token离过期提前请求时间（单位：秒）
// 被拦截的请求push到数组中
function subscribesArrRefresh(cb) {
  subscribesArr.push(cb);
}
// 用新token发起请求队列中的所有请求
function reloadSubscribesArr(newToken) {
  subscribesArr.map(cb => {
    //console.log("当前存储接口数为:" + subscribesArr.length);
    cb(newToken)
  });
}

//判断当前时间是否超过token过期时间
function isTokenExpire(serverTokenExpire) {
  return getUTCTime(new Date()) >= serverTokenExpire*1;
}

/**
 * http request 拦截器
 * 日期 ：2020/1/4
 ***/
axios.interceptors.request.use(
  config => {    
    let token = localStorage.getItem('token');// 获取当前token值    
    let validTime = localStorage.getItem('validTime');// 获取获取token有效时间
    // 是否需要拦截
    if (config.interceptor =="undefined" || !config.interceptor) {
      return config;
    }
    // 判断当前token是否逻辑失效
    // if (validTime =="undefined" ||isTokenExpire(validTime)) {
      if (false) {
      // 判断当前是否正在获取新token中
      if (!isTokenRefreshing) {
        isTokenRefreshing = true;
        // 请求token
        let tokenAPI="system/auth/getToken"
        tokenAPI=request.getIP(tokenAPI)
        axios.post(tokenAPI,{}, {
            headers: {
                'Access-Token': token
            }
        })
          .then(res => {
            if(res.status == 200){
              token = res.data.data.token;
              localStorage.setItem('token',token);
              let timestamp = (new Date()).getTime()+(res.data.data.validTime-tokenRequestTime)*1000;
              localStorage.setItem('validTime',timestamp);
              isTokenRefreshing = false;
              reloadSubscribesArr(token);
            }else{
              isTokenRefreshing = false;
              subscribesArr = [];
            }
          }).catch(err => {
            // 请求失败，清空缓存，返回登录页
            isTokenRefreshing = false;
            // Message.warning("登录超时，请重新登录。", 5);
            subscribesArr = [];
            router.replace({
              path: '/',
              query: {
                redirect: router.currentRoute.fullPath
              }
            })
          });
      }
      let retry = new Promise((resolve, reject) => {
        subscribesArrRefresh((newToken) => {
          config.headers["Access-Token"] = newToken;
          resolve(config);
        });
      });
      return retry;
    } else {
      config.headers["Access-Token"] = token;
      return config;
    }
  },
  error => {
    return Promise.reject(error);
  }
);


/**
 * http response 拦截器
 * 日期 ：2018/9/12 
 ***/
axios.interceptors.response.use(
  response => {
    let token = localStorage.getItem('token');// 获取当前token值    
    if (response) {
      switch (response.status) {
        case 400:
          console.error("错误信息:API请求参数错误:"+response.config.url)
          console.error(response.data.msg)
        break;
        case 403:
          // 返回 403 清除token信息并跳转到登录页面
          // store.commit(types.LOGOUT);
          if(token!=""){
            //Message.error("403:"+response.data.msg, 5);
          
            // router.replace({
            //   path: '/',
            //   query: {
            //     redirect: router.currentRoute.fullPath
            //   }
            // })
          }
          break;
      }
    }
    return response;
  },
  error => {
    if (error.response) {
      switch (error.response.status) {
        case 403:
          // 返回 403 清除token信息并跳转到登录页面
          store.commit(types.LOGOUT);
          // router.replace({
          //   path: '/',
          //   query: {
          //     redirect: router.currentRoute.fullPath
          //   }
          // })
      }
    }
    return error.response;
  }
)

export default axios;
